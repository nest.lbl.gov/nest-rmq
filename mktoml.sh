#!/bin/bash
#
# Processes *.toml.tmpl file to resolved variables

process_toml_tmpl() {
    output=${1%.tmpl}
    if [ "X${1}" != "X${output}" -a -f "${1}" ] ; then
        echo "Creating \"${output}\""
        eval "echo \"$(sed 's/"/\\"/g' ${1})\"" > ${output}
    fi
}

if [ 0 == $# ] ; then
    for tmpl in $(find . -name "*.toml.tmpl") ; do
        process_toml_tmpl ${tmpl}
    done
else
    for tmpl in "${@}" ; do
        process_toml_tmpl ${tmpl}
    done
fi
